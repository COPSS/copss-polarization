#!/bin/bash

#set -x


for i in {1..20}
do
   sed -e "s/epsilon 1/epsilon ${i}/g" base.interfaces > interfaces.in
   mpiexec -np 1 ../../../src/polarization-opt 
done

