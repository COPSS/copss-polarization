forces_anal = load('force_2sphers_analytical_solution.dat');
forces_icc  = load('forces_icc_p2.dat');

h=figure
hold on
plot(forces_anal(:,1), forces_anal(:,2),'r')
plot(forces_icc(:,1), forces_icc(:,4), 'b')

print(h,'compare_forces.png','-dpng')
