# Plot forces on dielectric particles

import numpy as np
import matplotlib.pyplot as plt

data_anal = np.loadtxt("force_2sphers_analytical_solution.dat",skiprows=0)
data_icc1 = np.loadtxt("forces_icc_p2.dat")

distance = data_anal[:,0]
force_anal = data_anal[:,1]

distance1 = data_icc1[:,0]
force_icc1 = data_icc1[:,3] 

plt.plot(distance,force_anal)
plt.xlabel("Distance")
plt.ylabel("Force")

plt.plot(distance1,force_icc1,marker='o',linestyle='None')

plt.savefig('compare_forces.png')

