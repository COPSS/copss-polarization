#!/bin/bash

#set -x


for (( i=3; i<=7; i=i+1 ))
##for i in {3..10}
do
   sed -e "s/position 10/position ${i}/g" base.interfaces > interfaces.in
   mpiexec -np 1 ../../../src/polarization-opt 
done

