# number of processors to use
ncpus = 1

import os
FNULL = open (os.devnull, 'w')

run = "mpiexec -np "+str(ncpus)+ " ../../src/polarization-opt"
cmd = run
print(cmd)
os.system(cmd)
