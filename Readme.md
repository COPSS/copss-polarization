**COPSS-Polarization 0.3.0**
==============================

**COPSS-Polarization** solves the electrostatic interactions in charged systems that exhibit sharp discontinuous changes in dielectric permittivity. It is based on an efficient $O(N)$ computational approach to model arbitrary-shaped sharp interfaces separating dielectrics with spatially different dielectric permittivities. A parallel boundary element Poisson solver is the center of the algorithm.


**Installation**
-------------------------------------------
**COPSS** is written using [libMesh](http://libmesh.github.io/) framework. It also requires [PETSc](https://www.mcs.anl.gov/petsc/index.html) for parallel linear equation solvers and [ScalFMM](https://gitlab.inria.fr/solverstack/ScalFMM) for fast multipole computations. Before installing **COPSS**, you need to install **PETSc**, **libMesh**, and **ScalFMM**. To achieve the best parallel performance of COPSS, we suggest install it on a Linux cluster environment.
###0. System environment prep
You should have the following software loaded or compiled

 - [CMAKE](https://cmake.org/) (version 2.8.12 or later)
 - [GCC](https://gcc.gnu.org/) (version 4.8.4 or later)
 - [PYTHON](https://www.python.org/) (python 2)
 - [OPENMPI](https://www.open-mpi.org/) (version 1.10.4 or later) or [MVAPICH](http://mvapich.cse.ohio-state.edu/) (version mvapich2 2.2b or later)

###1. Install PETSc
 - Download PETSC's latest release ( version 3.7.4 or later ) from [PETSc download](https://www.mcs.anl.gov/petsc/download/index.html), or git clone PETSc repository:
	 
	 `mkdir $HOME/projects/`
	 
	 `cd $HOME/projects/`
	 
	 `git clone -b maint https://bitbucket.org/petsc/petsc petsc`

 - Configure PETSc: 
 
	 `cd $HOME/projects/petsc`
	 
	 `./configure --with-cc=mpicc --with-cxx=mpicxx --with-mpiexec=mpiexec --with-fc=mpif90 --download-fblaslapack --download-scalapack --download-mumps --download-superlu_dist --download-hypre --download-ml --download-parmetis --download-metis --download-triangle --download-chaco --with-debugging=0`
	 
	 **Then follow the instructions on screen to install and test the package.**

 - Export environment variables:
	
	   `export PETSC_DIR=*/path/to/PETSC*`
	   `export PETSC_ARCH=*PETSC_ARCH_NAME*`
	
	  **Add the above codes to ~/.bashrc and `source ~/.bashrc` before next step. (`*/path/to/PETSC*` and `*PETSC_ARCH_NAME*` can be found on the screen after installation.)**

	If you meet any trouble, please refer to [PETSC installation](https://www.mcs.anl.gov/petsc/documentation/installation.html), or reach out to **PETSc** community for help.
	

###2. Install libMesh
 - Download libMesh's latest release ( version 1.1.0 or later ) from [libMesh download](https://github.com/libMesh/libmesh/releases), or git clone libMesh repository:
	 
	 `cd $HOME/projects/`
	 
	 `git clone git://github.com/libMesh/libmesh.git`
	 
 - Build libMesh: 
 
	 `cd $HOME/projects/libmesh`
	 
	 `./configure -prefix=$HOME/projects/libmesh/libmesh-opt --enable-optional --enable-vtk  --enable-gzstream --enable-trilinos --disable-strict-lgpl --enable-laspack --enable-capnproto --enable-nodeconstraint --enable-perflog --enable-ifem --enable-petsc --enable-blocked-storage --enable-unique-id --enable-unique-ptr --enable-parmesh 2>&1  | tee my_config_output_opt.txt`

	(Read the configuration output, make sure **PETSC** is enabled).
	 	 
 - Export environment variables:
	
	   `export LIBMESH_DIR=*/path/to/LIBMESH*`
	
	  **Add the above codes to ~/.bashrc and `source ~/.bashrc` before next step.

	If you meet any trouble, please refer to [libMesh installation](https://libmesh.github.io/installation.html), or reach out to **libMesh** community for help.


###3. Install ScalFMM
 - **COPSS-Polarization** houses its own version of ScalFMM that is specifically modified for the polarization problem. Download the latest version

	 `cd /path/to/where/you/want/to/install/copss`

	 `git clone https://bitbucket.org/COPSS/copss-polarization-public.git`
	 
	 `cd copss/contrib/scalfmm/`
	 
 - Configure ScalFMM:

	 `cd scalfmm/`

	 `mkdir Build`

	 `cd Build`

	 `cmake .. -DSCALFMM_USE_MPI=ON -DSCALFMM_USE_FFT=OFF -DSCALFMM_USE_SSE=OFF -DSCALFMM_USE_AVX=OFF -DCMAKE_INSTALL_PREFIX=../Install`

	 `make`

 	 `make install`

      If you meet any trouble, please refer to [ScalFMM Build](https://gitlab.inria.fr/solverstack/ScalFMM), or reach out to **ScalFMM** community for help.

###4. Install COPSS-Polarization

 - Compile the codes
 
	 `cd /path/to/copss/src/` 
	 
	 `make`
		
 - Run the system

	 `cp /path/to/copss/examples/sphere/2_spheres`

	`python run.py` (You can define how many cores you want to run on in **run.py**)
	
	You need to set up your system in **control.in** and **interfaces.in**. More details can be found in our documentation.


**Build documentation**
-------------------------------------------
After you have build **COPSS-Polarization** you can further build the documentation from doxygen/ directory. Make sure you have [Doxygen](http://www.stack.nl/~dimitri/doxygen/) ready:

    doxygen Doxyfile.bak

Then you can view the documentation in IE browser:

    google-chrome html.index.html

**What's next**
-------------------------------------------

 - Coupling COPSS with molecular dynamics package [LAMMPS](http://lammps.sandia.gov/) 
 - Coupling COPSS with hydrodynamics calculations ([COPSS-Hydrodynamics](https://bitbucket.org/COPSS/copss-hydrodynamics-public) developed by COPSS team).
 - More user friendly interface. 
 - etc...

**Contribution**
-------------------------------------------
Hydrodynamic and electrostatic interactions are ubiquitous in nature and technology, including but not limited to colloidal, biological, granular systems, etc. **COPSS** is trying to solve these problems in a efficient and scalable manner. Contributions on the code structure, parallel-performance, applications are much appreciated. 

**Contact**
-------------------------------------------
 
 -  If you need help, have any questions or comments, join our mailing list [copss-users@googlegroups.com](Link URL)

     **GMail users**: just click "Join group" button

     **Everyone else**: send an email to [copss-users+subscribe@googlegroups.com](Link URL)

**Algorithm**
-------------------------------------------
 Our method paper has details on the implementation and tests of this code: [An O(N) and parallel approach to integral problems by a kernel-independent fast multipole method: Application to polarization and magnetization of interacting particles](https://arxiv.org/abs/1605.01715)


**Main contributors**
-------------------------------------------
 
 - [**Xikai Jiang**](https://www.researchgate.net/profile/Xikai_Jiang), Institute for Molecular Engineering, The University of Chicago. [EMAIL](xikai@uchicago.edu)

 - [**Jiyuan Li**](https://scholar.google.com/citations?user=XE6JtJwAAAAJ&hl=en), Institute for Molecular Engineering, The University of Chicago. [LinkedIn](https://www.linkedin.com/in/jyliuchicago/), [EMAIL](jyli@uchicago.edu)
 
 

**License**
-------------------------------------------
 
* The codes are open-source and distributed under the GNU GPL license, and may not be used for any commercial or for-profit purposes without our permission.


**Release history of COPSS**
-------------------------------------------
 
 - 0.3.0 (Release COPSS-CMA-ES module)

 - 0.2.0 (Release COPSS-Hydrodynamics module)

 - 0.1.0 (Release COPSS-Polarization module)

