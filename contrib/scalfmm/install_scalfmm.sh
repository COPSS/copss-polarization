#!/bin/bash

# Installing ScalFMM in this directory.

cd scalfmm
mkdir Build
cd Build
cmake .. -DSCALFMM_USE_MPI=ON -DSCALFMM_USE_FFT=OFF -DSCALFMM_USE_SSE=OFF -DSCALFMM_USE_AVX=OFF -DCMAKE_INSTALL_PREFIX=../Install
make
make install

