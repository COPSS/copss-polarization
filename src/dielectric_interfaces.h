// Fast Multipole Boundary Element Method -- Electrostatic Polarization.
// Copyright (C) 2015-2017 Jiyuan Li, Xikai Jiang

// This code is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public
// License along with this code; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


/** @file dielectric_interfaces.h
 *  @brief Read in input parameters for dielectric interfaces, and manage its properties.
 *
 *  This file includes reading input parameters for dielectric interfaces from interfaces.in
 *  and managing parameters and properties of the dielectric interfaces.
 *  This file also include a Vector class that contains methods for vector manipulations.
 * 
 *  @author Jiyuan Li
 *  @author Xikai Jiang
 *  @bug No known bugs.
 */



#ifndef DIELECTRIC_INTERFACES_H
#define DIELECTRIC_INTERFACES_H

#include "stdio.h"

// libMesh include files
#include "libmesh/point.h"
#include "libmesh/id_types.h"
#include "libmesh/mesh.h"
//#include "Vector.h"
#include "type.h"

namespace libMesh
{
	/**
	 * DielectricInterfaces class for spherical interfaces
	 */
	class DielectricInterfaces : public ReferenceCountedObject<DielectricInterfaces>
	{
	private:
		/**
                 * Total number of interfaces
                 */
		const int _num;

                /**
		 * Centroid of all spherical interfaces (not center of mass)
		 */
		std::vector<Real> _center;

	public:
                /**
		 * Constructor
		 */
		DielectricInterfaces(const int num_of_interfaces);

                /**
		 * Destructor
		 */
		~DielectricInterfaces ();

		/**
		 * Radius of spherical interfaces 
		 */
		const std::vector<Real> _radius = interfaces(_num)[0];

		/**
		 * Free charge by spherical interfaces 
		 */
		const std::vector<Real> _free_charge = interfaces (_num)[1];

		/**
		 * Dielectric permittivity inside spherical interfaces 
		 */
		const std::vector<Real> _epsilon_in = interfaces (_num)[2];

		/**
		 * Center of each spherical interfaces 
		 */
		const std::vector<std::vector<Real> > _position = position();

		/**
                 * Return total number of interfaces
                 */
		const unsigned int num() {return _num; };

		/**
                 * Force initialization (all zeros)
                 */
		std::vector<std::vector<Real> > force_zero();

		/**
                 * Assign charge density sigma_f
                 */
		const std::vector<Real> sigma_f();

		/**
		 * Define a few functions for box-definition 
		 * "distance" (check dielectric_interfaces.C for how this distance
		 * is defined) between spherical interfaces
		 */
		Real distance(unsigned int , unsigned int);

		/**
		 * Maximum "distance" between spherical interfaces
		 */
		Real distance_max();

                /**
		 * Maxmize radius
		 */
		Real radius_max();

                /**
		 * Centroid of all spherical interfaces (not center of mass)
		 */
		std::vector<Real> center();

		/**
                 * Functions for reading-in data
		 * parameters of all interfaces
		 */
		std::vector<std::vector<Real> > interfaces(const int);

                /**
		 * Assemble position matrix
		 */
		std::vector<std::vector<Real> > position()
		{
			std::vector<std::vector<Real> > tmp;
			tmp.push_back(interfaces (_num)[3]); 
			tmp.push_back(interfaces (_num)[4]);
			tmp.push_back(interfaces (_num)[5]);
			return tmp;
		};

		/**
		 * Display information of the interface
		 */
		void print_info	(std::ostream & out = libMesh::out);
	
	};//end of dielectric_interfaces class
}

/**
 * Vector operation namespace
 */
using namespace libMesh;

namespace Vector
{
	void showVector(std::vector<Real> );

	void showMatrix(std::vector<std::vector<Real> > );

	std::vector<Real> add(std::vector<Real> , std::vector<Real> );

	std::vector<Real> sub(std::vector<Real> , std::vector<Real> );
	
	std::vector<Real> pointMult(std::vector<Real>, std::vector<Real> );

	std::vector<Real> multiConst(std::vector<Real>, Real);
	
	std::vector<Real> inverse(std::vector<Real> );
	
	std::vector<Real> pointDivide(std::vector<Real> , std::vector<Real> );

	Real max_elem(std::vector<Real> ) ;

} //end of Vector namespace


#endif

/*	dielectric_interfaces.h */
