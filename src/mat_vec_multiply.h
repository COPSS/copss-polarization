// Fast Multipole Boundary Element Method -- Electrostatic Polarization.
// Copyright (C) 2015-2017 Jiyuan Li, Xikai Jiang

// This code is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public
// License along with this code; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


/**
 * @file mat_vec_multiply.h
 * @brief Header file for matrix-vector multiplication
 *
 * @author Xikai Jiang
 * @author Jiyuan Li
 * @bug No known bugs
 */

#ifndef LIBMESH_POLARIZATION_SHELL_MATRIX_H
#define LIBMESH_POLARIZATION_SHELL_MATRIX_H

// Local includes
#include "libmesh/libmesh_common.h"
#include "libmesh/reference_counted_object.h"
#include "libmesh/libmesh.h"
#include "libmesh/equation_systems.h"
#include "libmesh/shell_matrix.h"

#include "dielectric_interfaces.h"
#include "type.h"


namespace libMesh
{

/**
 * @brief Class for matrix-vector multiplication
 *
 * This class performs matrix-vector multiplication used
 * by iterative solvers. The use of a shell matrix avoids
 * the explicit storage of global matrix, and enables the
 * O(N) memory cost.
 */

template <typename T>
class MatVecMultiply : public ShellMatrix<T>
{
public:
  /**
   * Constructor: takes references to the System.
   * the FMM kernel, and the rigid_particle.
   * System, FMM kernel, and dielectric_interfaces have
   * to be defined and stored elsewhere.
   *
   * @param Reference to the EquationSystem
   * @param Reference to the FMM kernel, for example 1/r
   * @param Reference to the DielectricInterfaces object
   */
  explicit
  MatVecMultiply (EquationSystems& new_bs, KernelClass& kernel, DielectricInterfaces& rp);

  /**
   * Destructor.
   */
  virtual ~MatVecMultiply ();

  /**
   * Get the total number of rows of the global matrix
   *
   * @return \p m, the row-dimension of the matrix where the marix is
   * \f$ M \times N \f$.
   */
  virtual numeric_index_type m () const;

  /**
   * Get the total number of columns of the global matrix
   *
   * @return \p n, the column-dimension of the matrix where the marix
   * is \f$ M \times N \f$.
   */
  virtual numeric_index_type n () const;

  /**
   * Performs matrix-vector multiplication.
   * Multiplies the matrix with \p arg and stores the result in \p
   * dest.
   *
   * @param Destination vector
   * @param Source vector
   * @return None
   */
  virtual void vector_mult (NumericVector<T>& dest,
                            const NumericVector<T>& arg) const;

  /**
   * Performs matrix-vector multiplication and add the results to
   * the global matrix.
   * Multiplies the matrix with \p arg and adds the result to \p dest.
   *
   * @param Destination vector
   * @param Source vector
   * @return None
   */
  virtual void vector_mult_add (NumericVector<T>& dest,
                                const NumericVector<T>& arg) const;

  /**
   * Copies the diagonal part of the matrix into \p dest.
   *
   * @param Destination vector
   * @return None
   */
  virtual void get_diagonal (NumericVector<T>& dest) const;

protected:
  /**
   * Reference to the EquationSystem.
   */
  EquationSystems& _bs;

  /**
   * Reference to the FMM kernel.
   */
  KernelClass& _kernel;

  /**
   * Reference to the DielectricInterfaces.
   */
  DielectricInterfaces& _rp;
};



//---------------------------------------
// MatVecMultiply inline members

/**
 * Constructor
 */
template <typename T>
inline
MatVecMultiply<T>::MatVecMultiply (EquationSystems& new_bs, KernelClass& kernel, DielectricInterfaces& rp):
ShellMatrix<T>(new_bs.comm()), _bs(new_bs),
  _kernel(kernel),
  _rp(rp)
{}


/**
 * Destructor
 */
template <typename T>
inline
MatVecMultiply<T>::~MatVecMultiply ()
{}


/**
 * Get total number of rows of global matrix
 */
template <typename T>
inline
numeric_index_type MatVecMultiply<T>::m () const
{
  /**
   * The global matrix is N*N, where N is
   * the number of degrees of freedoms.
   */
  return _bs.n_dofs();
}


/**
 * Get total number of columns of global matrix
 */
template <typename T>
inline
numeric_index_type MatVecMultiply<T>::n () const
{
  /**
   * The global matrix is N*N, where N is
   * the number of degrees of freedoms.
   */
  return _bs.n_dofs();
}

} // namespace libMesh

#endif // LIBMESH_POLARIZATION_SHELL_MATRIX_H
