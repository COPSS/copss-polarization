// Fast Multipole Boundary Element Method -- Electrostatic Polarization.
// Copyright (C) 2015-2017 Jiyuan Li, Xikai Jiang

// This code is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public
// License along with this code; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


/** @file main.C
 *  @brief The main driver function.
 *
 *  This is the main function that read input files,
 *  create EquationSystems, solve the system, calculate
 *  the body forces, and output Exodus files for
 *  visualization purpose.
 *
 *  @author Jiyuan Li
 *  @author Xikai Jiang
 *  @bug No known bugs.
 */

// C++ include files.
#define _USE_MATH_DEFINES
#include <iostream>
#include <math.h>

// Basic include files needed for the mesh functionality.
#include "libmesh/libmesh.h"
#include "libmesh/perf_log.h"
#include "libmesh/mesh.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/explicit_system.h"
#include "libmesh/equation_systems.h"
#include "libmesh/mesh_refinement.h"

// Finite Element object.
#include "libmesh/fe.h"

// Gauss quadrature rules.
#include "libmesh/quadrature_gauss.h"

// Datatypes for finite element matrix and vector components.
#include "libmesh/sparse_matrix.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/elem.h"
#include "libmesh/vector_value.h"
#include "libmesh/tensor_value.h"

// DofMap handles degree of freedom indexing.
#include "libmesh/dof_map.h"

// Read command line tool.
#include "libmesh/getpot.h"

// ScalFMM.
#include "Utils/FPoint.hpp"
#include "Core/FFmmAlgorithmTsm.hpp"

// Polarization classes
#include "mat_vec_multiply.h"
#include "polarization_system.h"
#include "type.h"
#include "dielectric_interfaces.h"

using namespace libMesh;

/**
 * @brief Main Function
 *
 *  This is the main function that read input files,
 *  create EquationSystems, solve the system, calculate
 *  the body forces, and output Exodus files for
 *  visualization purpose.
 *
 * @return Return 0
 */
int main (int argc, char** argv)
{
  // Initialize libMesh library and MPI.
  LibMeshInit init (argc, argv);

  // Skip this 3D problem if libMesh was compiled as 1/2D-only.
  libmesh_example_requires(3 <= LIBMESH_DIM, "3D support");
  
  // Read input from input file
  GetPot input_file("control.in");

  // Mesh
  std::string meshin = input_file ("meshin", "Inputs/two_sphere_surfaces_1004elem_tri3.e");

  // Mesh refinement level
  unsigned int r_steps = input_file ("r_steps" , 0);

  // GMRES convergence 
  const Real eps = input_file ("eps", 1.e-4);

  // Medium dielectric constant
  const Real epsilon_out = input_file ("medium_dielectric", 1.);

  // FMM control parameter
  unsigned int TreeHeight = input_file  ("TreeHeight", 3);
  unsigned int SubTreeHeight = input_file ("SubTreeHeight", 1);

  // Total number of interfaces
  const unsigned int num_tot = input_file ("num_tot", 2);

  // Interface shape
  std::string interface_shape = input_file ("interface_shape", "flat");

  // Use FMM or Direction Summation; use self correction for singularity in boundary integral or not
  bool with_fmm             = input_file ("with_fmm", true);
  bool with_self_correction = input_file ("with_self_correction", true);

  // Define dielectric_interfaces object  
  DielectricInterfaces dielectric_interfaces (num_tot);

  // Define box size and box center
  Real boxWidth = 2 * (dielectric_interfaces.distance_max() + dielectric_interfaces.radius_max() ) + 0.2; 
  std::vector<Real> center_of_box = dielectric_interfaces.center();
  Point CenterOfBox (center_of_box[0], center_of_box[1], center_of_box[2]);
  FPoint <FReal> centerOfBox (center_of_box[0], center_of_box[1], center_of_box[2]);

  // Performance log
  PerfLog perf_log ("Polarization");

  // Create a mesh, with dimension to be overridden later, distributed
  // across the default MPI communicator.
  Mesh mesh(init.comm());
  perf_log.push("read-in-mesh");
  mesh.read(meshin);
  perf_log.pop("read-in-mesh");
  
  // Mesh refinement before attach mesh to equation system
  MeshRefinement mesh_refinement(mesh);

  // Refine r_steps times
  for (unsigned int i=0; i!=r_steps; ++i)
  {
      perf_log.push("refine-mesh");
      mesh_refinement.uniformly_refine(1);
      perf_log.pop("refine-mesh");
  }

  // Perform ScalFMM compression and set M2L operators
  KernelClass kernel(TreeHeight,boxWidth,centerOfBox,&MatrixKernel);

  // Equation system 2D surface
  EquationSystems dielectric_system (mesh);

  // Attach global parameters to the system 
  dielectric_system.parameters.set<Real>         ("dielectric permittivity of medium") = epsilon_out;
  dielectric_system.parameters.set<Real>         ("linear solver tolerance") = eps;
  dielectric_system.parameters.set<unsigned int> ("TreeHeight") = TreeHeight;
  dielectric_system.parameters.set<unsigned int> ("SubTreeHeight") = SubTreeHeight;
  dielectric_system.parameters.set<Real>         ("BoxWidth") = boxWidth;
  dielectric_system.parameters.set<Point>        ("CenterOfBox") = CenterOfBox;
  dielectric_system.parameters.set<std::string>  ("InterfaceShape") = interface_shape;
  dielectric_system.parameters.set<bool>         ("with_fmm") = with_fmm;
  dielectric_system.parameters.set<bool>         ("with_self_correction") = with_self_correction;

  // Create LinearImplicitSystem
  LinearImplicitSystem & system_bc = dielectric_system.add_system<LinearImplicitSystem> ("BoundaryCharge");

  // Create ExplicitSystem for Visualization only to save memory
  // because it doesn't need the global matrix
  dielectric_system.add_system<ExplicitSystem> ("Visualization");

  // Add variables for boundary and total charge density, and element area
  system_bc.add_variable("sigma_b", CONSTANT, MONOMIAL);
  system_bc.add_vector("sigma_f", false);
  system_bc.add_vector("area", false);

  // We need an additional matrix to be used for preconditioning since
  // a shell matrix is not suitable for that.
  system_bc.add_matrix("Preconditioner");

  // Attach the PolarizationSystem to assemble rhs
  PolarizationSystem pz(dielectric_system, kernel, dielectric_interfaces);
  system_bc.attach_assemble_object(pz);

  // Add total surface charge density and element area for visualization
  dielectric_system.get_system("Visualization").add_variable("sigma_f", CONSTANT, MONOMIAL);
  dielectric_system.get_system("Visualization").add_variable("area", CONSTANT, MONOMIAL);

  // Initialize equation system
  dielectric_system.init();

  // Move meshes to locations defined in interfaces.in
  pz.move_mesh_to_input_locations(); 

  // Calculate element area
  if (strcmp(interface_shape.c_str(), "sphere") == 0){ // spherical patch area.
     pz.area_spherical_tri();
  }
  else if (strcmp(interface_shape.c_str(), "flat") == 0){ // flat patch area.
     pz.area_flat_element();
  }

  // Set user defined free surface charge density
  pz.user_init_free_surface_charge();

  // ShellMatrix for matrix-vector multiplication
  MatVecMultiply<Number> shellMatrix(dielectric_system, kernel, dielectric_interfaces);

  // Attach shell matrix to BoundaryCharge system
  system_bc.attach_shell_matrix(&shellMatrix);

  // Reset the preconditioning matrix to zero (for the system matrix,
  // the same thing is done automatically).
  system_bc.get_matrix("Preconditioner").zero();

  perf_log.push("solve");

  // Assemble & solve the linear system
  system_bc.solve();

  // Detach the shell matrix from the system since it will go out of
  // scope. Nobody should solve the system outside this function.
  system_bc.detach_shell_matrix();

  // Print how many steps for GMRES to converge, and the residual.
  std::cout << "Solved linear system in " << system_bc.n_linear_iterations() << " iterations, residual norm is " << system_bc.final_linear_residual() << "." << std::endl;

  // Calculate force on one sphere.
  pz.get_forces_energy();
  perf_log.pop("solve");

  // Transfer variables to visualization system.
  pz.visualize();

  // Output mesh and variable nodal values to Exodus file
  perf_log.push("output");
  ExodusII_IO exo_io(mesh);
  exo_io.write("output.e");
  exo_io.write_element_data(dielectric_system);
  perf_log.pop("output");
  
  return 0;
}
