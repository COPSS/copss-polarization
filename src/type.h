// Fast Multipole Boundary Element Method -- Electrostatic Polarization.
// Copyright (C) 2015-2017 Jiyuan Li, Xikai Jiang

// This code is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public
// License along with this code; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


/** @file type.h
 *  @brief Mostly include ScalFMM header files.
 *
 *  This file includes mainly ScalFMM header files.
 *  They will be used in functions that use ScalFMM
 *  kernels and perform FMM computations.
 *
 *  @author Jiyuan Li
 *  @author Xikai Jiang
 *  @bug No known bugs.
 */

#ifndef TYPE_H
#define TYPE_H

#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>

// ScalFMM header files.
#include "Components/FTypedLeaf.hpp" //contains "FtypedLeaf" class
#include "Containers/FOctree.hpp" //contains "FOctree" class
#include "Kernels/Chebyshev/FChebCell.hpp"//contains "FTypedChebCell" class
#include "Kernels/P2P/FP2PParticleContainerIndexedWithNormal.hpp"//contains "FP2PParticleContainerIndexedWithNormal" class
#include "Kernels/Interpolation/FInterpMatrixKernel.hpp"//contains "FInterpMatrixKernelR" class
#include "Kernels/Chebyshev/FChebKernel.hpp"//contains "FChebKernel" class

using namespace std;

// Global constants and variables
const double PI_4 = 4. * M_PI;

// SCALFMM definitions.
typedef double FReal;

// In order to be used in a ScalFMM template, a constant value must be initialized.
const unsigned int ORDER = 6;

// ScalFMM particle container, leaf, cell, and octree
typedef FP2PParticleContainerIndexedWithNormal<FReal>     ContainerClass;
typedef FTypedLeaf<FReal,ContainerClass>                  LeafClass;
typedef FTypedChebCell<FReal,ORDER>			  CellClass;
typedef FOctree<FReal,CellClass,ContainerClass,LeafClass> OctreeClass;

// Kernel 1/r
typedef FInterpMatrixKernelR<FReal> MatrixKernelClass;

// Kernel class
typedef FChebKernel<FReal,CellClass,ContainerClass,MatrixKernelClass,ORDER> KernelClass;
const MatrixKernelClass MatrixKernel;

#endif
