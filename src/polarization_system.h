// Fast Multipole Boundary Element Method -- Electrostatic Polarization.
// Copyright (C) 2015-2017 Jiyuan Li, Xikai Jiang

// This code is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// This code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public
// License along with this code; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


/**
 * @file polarization_system.h
 * @brief Provide functions to manipulate the polarization system
 *
 * @author Xikai Jiang
 * @author Jiyuan Li
 * @bug No known bugs
 */

#ifndef POLARIZATION_SYSTEM_H
#define POLARIZATION_SYSTEM_H

// libMesh includes
#include "libmesh/libmesh.h"
#include "libmesh/mesh.h"
#include "libmesh/libmesh_common.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/explicit_system.h"
#include "libmesh/dof_map.h"
#include "libmesh/reference_counted_object.h"
#include "libmesh/equation_systems.h"
#include "libmesh/shell_matrix.h"

#include "dielectric_interfaces.h"
#include "mat_vec_multiply.h"
#include "type.h"

namespace libMesh
{

/**
 * This class is inherited from libMesh's Assembly class.
 * It can assemble the right-hand-side (rhs), calculate element area,
 * prepare data for visualization, and calculate body forces.
 */
class PolarizationSystem : public System::Assembly
{
public:
  /**
   * Constructor: takes references to the System.
   * the FMM kernel, and the dielectric_interface.
   * System, FMM kernel, and dielectric_interfaces have
   * to be defined and stored elsewhere.
   *
   * @param Reference to the EquationSystem
   * @param Reference to the FMM kernel, for example 1/r
   * @param Reference to the DielectricInterfaces object
   */
  PolarizationSystem (EquationSystems &bs_in,
  		      KernelClass     &kernel_in,
                      DielectricInterfaces  &rp_in);

  /**
   * Destructor.
   */
  ~PolarizationSystem ();

  /**
   * Assemble the right-hand-side (rhs) of the linear system of equations
   * for the boundary element method.
   */
  void assemble();

  /**
   * Assign user-defined free surface charge density to EquationSystem's vector sigma_f.
   */
  void user_init_free_surface_charge();

  /**
   * Calculate surface area for each element,
   * works for spherical interfaces with triangular elements.
   */
  void area_spherical_tri();

  /**
   * Calculate surface area for each element,
   * works for any flat element such as triangles, quadrilaterals.
   */
  void area_flat_element();

  /**
   * Prepare data for visualization of surface charges and element area.
   */
  void visualize();

  /**
   * Calculate body force for each interface.
   */
  void get_forces_energy();

  /**
   * Move meshes based on interface locations in interfaces.in.
   * Particle positions in input mesh MUST be (0, 0, 0)
   */
  void move_mesh_to_input_locations();

  /**
   * Move interfaces. To be implemented.
   */
  //void move_interface();

  /**
   * Return a pointer with outward normal vector to the input element.
   * Works for arbitrary shaped interfaces.
   */
  Point get_normal_vector(const Elem* elem);


private:
  /**
   * Reference to the EquationSystem.
   */
  EquationSystems& bs;

  /**
   * Reference to the FMM kernel.
   */
  KernelClass& kernel;

  /**
   * Reference to the DielectricInterfaces.
   */
  DielectricInterfaces& rp;
  
  /**
   * Particle ID
   */
  unsigned int interface_id;

  /**
   * Body forces
   */
  std::vector<std::vector<Real> > forces;

  /**
   * Electrostatic interaction energy
   */
  Real electrostatic_energy;

  /**
   * A constant reference to the whole mesh.
   */
  const MeshBase& mesh_bs = bs.get_mesh();

  /**
   * Number of total elements in the whole mesh.
   */
  const dof_id_type N_elem = mesh_bs.n_active_elem();

  /**
   * Number of elements per interface.
   * It is assumed here that the number of elements on
   * every sphere is the same.
   */
  const dof_id_type N_elem_per_sphere = N_elem / rp.num();

  /**
   * Number of local elements on this processor.
   */
  const dof_id_type N_local_elem = mesh_bs.n_local_elem();

  /**
   * A reference to the LinearImplicitSystem.
   * The linear system of equations to be solved.
   */
  LinearImplicitSystem& system_bs =
  bs.get_system<LinearImplicitSystem> ("BoundaryCharge");

  /**
   * A reference to the ExplicitSystem for visualization.
   */
  ExplicitSystem&  system_vl =
  bs.get_system<ExplicitSystem> ("Visualization");

  /**
   * A reference to the DofMap (Mapping for degrees of freedoms)
   * for linear system of equations and the visualzation system.
   */
  const DofMap& dof_map_bs = system_bs.get_dof_map();
  const DofMap& dof_map_vl = system_vl.get_dof_map();

  /**
   * Define dof_indices holders.
   */
  std::vector<dof_id_type> dof_indices_variable,
                           dof_indices_sigma_b,
                           dof_indices_sigma_f,
                           dof_indices_area;

  /**
   * Dielectric constant of the background medium.
   */
  const Real eps_out         = bs.parameters.get<Real> ("dielectric permittivity of medium");

  /**
   * FMM parameters -- tree height, box size, and box center.
   */
  unsigned int TreeHeight    = bs.parameters.get<unsigned int>("TreeHeight");
  unsigned int SubTreeHeight = bs.parameters.get<unsigned int>("SubTreeHeight");
  Real boxWidth              = bs.parameters.get<Real>("BoxWidth");
  Point CenterOfBox          = bs.parameters.get<Point>("CenterOfBox");

  /**
   * Particle's shape, currently support spherical interfaces
   * and interfaces with flat element.
   */
  std::string interface_shape = bs.parameters.get<std::string>("InterfaceShape");


  /**
   * Use FMM acceleration or use Direct Summation
   */
  bool with_fmm = bs.parameters.get<bool>("with_fmm");


  /**
   * Use self correction for singularity in boundary integral or not
   */
  bool with_self_correction = bs.parameters.get<bool>("with_self_correction");


  /**
   * Get variable number for the bound surface charge density.
   */
  const unsigned short int variable_number_sigma_b = system_bs.variable_number("sigma_b");
};

} // End of libMesh namespace

#endif
